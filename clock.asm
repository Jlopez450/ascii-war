        clr d7
		clr d6
		move.b #$01, d6         ;BCD add by d6 (#1)
		move.b vblanks, d7
		abcd d6,d7              ;add binary coded decimal
        move.b d7, vblanks		;store result
	    cmp.b region, d7        ;see if we should a second
		 beq add_second
        rts	
add_second:
        move.b #$00, vblanks	
		move.b #$01, d6	
		move.b seconds, d7	
		abcd d6, d7
		move.b d7, seconds 		
		cmpi.b #$60, seconds
		 beq add_minute
		rts
add_minute:
        move.b #$00, seconds		
		move.b #$01, d6	
		move.b minutes, d7	
		abcd d6, d7
		move.b d7, minutes		 
		cmpi.b #$60, minutes
		 bge reset_clock
		rts
reset_clock:
        move.b #$00,vblanks	
        move.b #$00,seconds		
        move.b #$00,minutes	
		rts					
