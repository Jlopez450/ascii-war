font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin" ;for easy display of hex characters
 
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8238	;field A    
	dc.w $8300	;$833e	
	dc.w $8407	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3c  ;h scroll		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200

title_art:
 dc.b "     __      _____   ____  _____  _____                         "
 dc.b "    /  \    /       /       | |    | |                          "
 dc.b "   /----\   \____  |        | |    | |                          "
 dc.b "  /      \       \ |        | |    | |                          "
 dc.b " /        \ _____/  \____  _|_|_  _|_|_                         "
 dc.b "                               ____                             "
 dc.b "   \                /  /\     /    \                            "
 dc.b "    \      /\      /  /  \    |____|                            "
 dc.b "     \    /  \    /  /----\   |\                                "
 dc.b "      \  /    \  /  /      \  | \                               "
 dc.b "       \/      \/  /        \ |  \_                             "
 dc.b "                                                                "
 dc.b "          Press START to begin                                  " 
 dc.b "                                                                "
 dc.b "  Press START+C for high contrast mode                          "
 dc.b "                                                                "
 dc.b "         Press A or b for info                                  "
 dc.b "          _                  v                                  "
 dc.b "  **     /o\                /o\     /\                          "
 dc.b "  /\     \~/ *            * \-/    /xx\                         "
 dc.b " /  \    _|_/              \_|_    \xx/                         "
 dc.b " |++|   / |                  | \    ++                          "
 dc.b " |++|  *  |                  |  *  -||-                         "
 dc.b " /  \    / \                / \     ||                          "
 dc.b " ||||   /   \              /   \    /\                          " 
 dc.b " ||||                              /||\                         "
 dc.b "                     ", $20,$7f, "2015 James Lopez                 "
 dc.b "                                                                "

about_screen: 
 dc.b "                                                                " 
 dc.b "  There lives an evil warlord from a                            "
 dc.b " dimension where all matter is made up                          "
 dc.b " of bad ASCII art. He conquered all                             "
 dc.b " ASCII land, but was not satisfied. In                          "
 dc.b " order to claim more land, he sent out                          "
 dc.b " an interdimensional scout to find new                          "
 dc.b " lands to conquer. A small resistance                           "
 dc.b " group sent out a craft of their own to                         "
 dc.b " chase down and destroy the scout,                              "
 dc.b " and spare the innocent from the evil                           " 
 dc.b " warlord's wrath. The epic battle that                          "
 dc.b " ensued would become known as:                                  "
 dc.b "-------------The-ASCII-war--------------                        "
 dc.b "             **Controls**                                       "  
 dc.b " Use the D pad for movement.'A' shoots,                         " 
 dc.b " the B button locks movement vertically                         "  
 dc.b " and C locks movement horizontally.                             " 
 dc.b "              **Credits**                                       " 
 dc.b " Music: SEGA, Tomomi Otani,                                     "
 dc.b " Howard Drossin, Michiru Yamane,                                "
 dc.b " Masanori Oouchi, Masanori Adachi,                              " 
 dc.b " Hiroshi Kobayashi, James Lopez,                                " 
 dc.b " Hiroto Kanno, Fumito Tamayama.                                 "   
 dc.b " Explosion sound clip: freesfx.co.uk                            " 
 dc.b " Everything else: James Lopez                                   " 
 dc.b "  Lines of 68k assembly: Approx. 2000                           "
 dc.b "    Program build date: MAY 26 2015                             "

bad_ending:
 dc.b "                                                                " 
 dc.b "   *Player 2 has defeated player 1!*                            " 
 dc.b "                                                                " 
 dc.b "   Despite the resistance group's best                          " 
 dc.b " efforts, the evil warlord is                                   " 
 dc.b " victorious. Yet another dimension has                          " 
 dc.b " fallen victim to his violent conquest.                         " 
 dc.b "                                                                " 
 dc.b "   Will you try again? Will you be                              " 
 dc.b " victorious, and spare this dimension                           " 
 dc.b " from pain and suffering?                                       " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "       _                     v    \ /                           "
 dc.b "      /o\!  <------   ===/  /o\   o o                           "
 dc.b "    * \~/ *              |* \-/  \___/                          "
 dc.b "     \_|_/               | \_|                                  "
 dc.b "       |                     |\                                 "
 dc.b "       |                     | *                                "
 dc.b "      / \                   / \                                 "
 dc.b "     /   \                 /   \                                " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "        Thank you for playing!                                  " 
 dc.b "    Press START to restart the game                             " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 
good_ending:
 dc.b "                                                                " 
 dc.b "   *Player 1 has defeated player 2!*                            " 
 dc.b "                                                                " 
 dc.b "   This dimension is safe. For now...                           " 
 dc.b "  The evil warlord could send out more                          " 
 dc.b "  scouts at any moment. Will you stay                           " 
 dc.b "  and defend against them?                                      " 
 dc.b "                                                                " 
 dc.b "                                                                "  
 dc.b "                                                                " 
 dc.b "               __---__                                          " 
 dc.b "              /   |   \                                         " 
 dc.b "             /    |    \                                        " 
 dc.b "             |    |    |                                        " 
 dc.b "             |    |    |                                        " 
 dc.b "             |    ^    |                                        " 
 dc.b "             |   /|\   |                                        " 
 dc.b "             \  / | \  /                                        "  
 dc.b "              \/  |  \/                                         " 
 dc.b "               -------                                          " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 dc.b "        Thank you for playing!                                  " 
 dc.b "    Press START to restart the game                             " 
 dc.b "                                                                " 
 dc.b "                                                                " 
 
 
pal_message:
 dc.b "  Sorry for the boarder, PAL users. At                          "
 dc.b " least the timer runs at correct speed!                         "
 

good_text:
 dc.b "Die monster! You don't belong in this world!"
 dc.b "Die monster! You don't belong in this world!"
 
evil_text:
 dc.b "All your dimension are belongs to us!" 
 dc.b "All your dimension are belongs to us!" 
;***************************************************************	
;Palettes
palette: ;main
	dc.w $0000,$0000,$0000,$0eee,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0eee,$0000,$0000,$0000,$0000 ;background place holder
	dc.w $0E0E,$0eee,$0eee,$0eee,$0eee,$0eee,$0eee,$0eee
	dc.w $0E0E,$0eee,$0eee,$0eee,$0eee,$0eee,$0eee,$0eee
	dc.w $0E0E,$0666,$0444,$000A,$0222,$0000,$006E,$0AAA
	dc.w $000E,$0000,$0000,$0000,$0000,$0000,$0000,$0000 ;player 2
	dc.w $0000,$0000,$0000,$0eee,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0eee,$0000,$0000,$0000,$0000 ;text
palette_desk:
	dc.w $0662,$0666,$0662,$0866,$0000,$0622,$0AAA,$0262
	dc.w $0A86,$0486,$0226,$046C,$06AA,$0626,$0266,$086C
palette_windmill:
	dc.w $0CCA,$0CCA,$0EEE,$0AA8,$0A66,$0446,$0244,$046A
	dc.w $068A,$08AC,$0CAA,$0ACC,$022C,$0224,$0422,$0000
palette_rocks:
	dc.w $0CCE,$08AC,$0222,$0446,$0668,$06AC,$0AAA,$0ACE
	dc.w $0CCE,$0024,$0246,$0864,$0420,$0642,$0A86,$0CA8
palette_logs:
	dc.w $0444,$0444,$0000,$0866,$0668,$068A,$08AC,$0468
	dc.w $0EEE,$0AAA,$0246,$0224,$0ACE,$0442,$0A86,$0422
palette_house:
	dc.w $0666,$0442,$0666,$0000,$0244,$0668,$0024,$0688
	dc.w $0AAA,$08AC,$0ACC,$0866,$0CAA,$0420,$0EEE,$0868
palette_cat:
	dc.w $0000,$0000,$0222,$0026,$0246,$0468,$068A,$0666
	dc.w $06AE,$0AAE,$088A,$0ACE,$0EAA,$0888,$0444,$0CEE
palette_phones:
	dc.w $0EEE,$0EEE,$0ACC,$0AAA,$0666,$0444,$0442,$0224
	dc.w $0000,$0466,$0886,$0CAA,$0468,$068A,$08AC,$0246
palette_cactus:
	dc.w $06AA,$06AA,$0666,$08AA,$0668,$0246,$08AC,$0ACE
	dc.w $0AAA,$0222,$0CEE,$0888,$0226,$026A,$028C,$06AC	
palette_sun:
	dc.w $0446,$044A,$0446,$024A,$046E,$022A,$0226,$0006
	dc.w $0444,$0002,$04AE,$0EEE,$024C,$0000,$0222,$0022
splash_palette:
	dc.w $0000,$0000,$0662,$0222,$0200,$0686,$0666,$0EC8
	dc.w $0886,$0888,$0C86,$0C82,$0EEE,$06CC,$0CC6,$08CE		
;***************************************************************	
;Ship sprites	
sprites:
	 incbin "gfx/p1tophoriz.bin"
	 incbin "gfx/p1bottomhoriz.bin"	;player 1
	 incbin "gfx/p1top.bin"
	 incbin "gfx/p1bottom.bin"
	 
	 incbin "gfx/p2tophoriz.bin"
	 incbin "gfx/p2bottomhoriz.bin"	;player 2
	 incbin "gfx/p2top.bin"
	 incbin "gfx/p2bottom.bin"	  
;***************************************************************
;Kosinski compressed background art
bg_desk:
	incbin "gfx/desk.kos"
bg_windmill:	
	incbin "gfx/windmill.kos"
bg_rocks:
	incbin "gfx/rocks.kos"
bg_logs:
	incbin "gfx/logs.kos"
bg_house:
	incbin "gfx/house.kos"
bg_cat:
	incbin "gfx/cat.kos"
bg_headphone:
	incbin "gfx/phones.kos"
bg_cactus:
	incbin "gfx/cactus.kos"	
bg_sun:
	incbin "gfx/sun.kos"
splashscreen:
	incbin "gfx/splash.kos"
;***************************************************************
;VGM format music	
rocketknightVGM:
	incbin "sound/rka11.vgm"
columnsVGM:
	incbin "sound/columns3.vgm"		
castlevaniaVGM:	
 	incbin "sound/vampire15.vgm"
decapVGM:	
 	incbin "sound/decap03.vgm"
clotho:	
 	;incbin "sound/clotho.vgm"	
 	incbin "sound/tetris.vgm"	
surferVGM:	
 	;incbin "sound/surfer.vgm"
 	incbin "sound/ThunderForceII06.vgm"
sttVGM:	
 	incbin "sound/stt.vgm"
vampire7VGM:
 	incbin "sound/vampire07.vgm"
comixVGM:
 	incbin "sound/CZB25.vgm"
oozeVGM:	
 	incbin "sound/ooze_2.vgm"		
endVGM:	
 	incbin "sound/gf2.vgm"
;***************************************************************		
boom:	
 	incbin "sound/boom.pcm" ;explosion PCM samples