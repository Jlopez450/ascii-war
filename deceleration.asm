p1_deceleration
		cmpi.b #$00,inverter
		beq return		
		move.b d3,d7
		andi.b #$0f,d7
		cmpi.b #$0f,d7
		bne return  ;are we still pressing the d-pad?
		bsr p1_decel_horizontal
		bsr p1_decel_vertical
		rts
		
p1_decel_horizontal:		
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p1_rtvel,d0
		move.w p1_ltvel,d1
		cmp d0,d1
		bgt p1_decel_left  
		bra p1_decel_right		
p1_decel_right:	
		cmpi.w #$0000,p1_rtvel
		beq nodecel	
		sub.w d1,d0
		sub.w #$0001,d0
        move.w d0,p1_rtvel
		move.w #$0000,p1_ltvel		
		movem.l (sp)+,d0/d1	
		rts	
p1_decel_left:
		cmpi.w #$0000,p1_ltvel
		beq nodecel	
		sub.w d0,d1
		sub.w #$0001,d1
        move.w d1,p1_ltvel
		move.w #$0000,p1_rtvel		
		movem.l (sp)+,d0/d1	
		rts
p1_decel_vertical:		
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p1_upvel,d0
		move.w p1_dnvel,d1
		cmp d0,d1
		bgt p1_decel_down
		bra p1_decel_up
		
p1_decel_up:	
		cmpi.w #$0000,p1_upvel
		beq nodecel	
		sub.w d1,d0
		sub.w #$0001,d0
        move.w d0,p1_upvel
		move.w #$0000,p1_dnvel		
		movem.l (sp)+,d0/d1	
		rts		
p1_decel_down:
		cmpi.w #$0000,p1_dnvel
		beq nodecel	
		sub.w d0,d1
		sub.w #$0001,d1
        move.w d1,p1_dnvel
		move.w #$0000,p1_upvel		
		movem.l (sp)+,d0/d1	
		rts			

p2_deceleration
		cmpi.b #$00,inverter
		beq return		
		move.b d3,d7
		andi.b #$0f,d7
		cmpi.b #$0f,d7
		bne return  ;are we still pressing the d-pad?
		bsr p2_decel_horizontal
		bsr p2_decel_vertical
		rts
		
p2_decel_horizontal:		
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p2_rtvel,d0
		move.w p2_ltvel,d1
		cmp d0,d1
		bgt p2_decel_left  
		bra p2_decel_right		
p2_decel_right:	
		cmpi.w #$0000,p2_rtvel
		beq nodecel	
		sub.w d1,d0
		sub.w #$0001,d0
        move.w d0,p2_rtvel
		move.w #$0000,p2_ltvel		
		movem.l (sp)+,d0/d1	
		rts	
p2_decel_left:
		cmpi.w #$0000,p2_ltvel
		beq nodecel	
		sub.w d0,d1
		sub.w #$0001,d1
        move.w d1,p2_ltvel
		move.w #$0000,p2_rtvel		
		movem.l (sp)+,d0/d1	
		rts
p2_decel_vertical:		
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p2_upvel,d0
		move.w p2_dnvel,d1
		cmp d0,d1
		bgt p2_decel_down
		bra p2_decel_up
		
p2_decel_up:	
		cmpi.w #$0000,p2_upvel
		beq nodecel	
		sub.w d1,d0
		sub.w #$0001,d0
        move.w d0,p2_upvel
		move.w #$0000,p2_dnvel		
		movem.l (sp)+,d0/d1	
		rts		
p2_decel_down:
		cmpi.w #$0000,p2_dnvel
		beq nodecel	
		sub.w d0,d1
		sub.w #$0001,d1
        move.w d1,p2_dnvel
		move.w #$0000,p2_upvel		
		movem.l (sp)+,d0/d1	
		rts		
nodecel:
		movem.l (sp)+,d0/d1	
		rts			
				
		