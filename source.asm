    include "ramdat.asm"
	include "header.asm"
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700,sr
		bsr clear_regs
		bsr clear_ram
		move.b #$04,p1_shooting
		move.b #$04,p2_shooting
	    bsr kill_psg
		move.w #$00a8,p1_xpos
		move.w #$0098,p1_ypos
		move.b #$01,p1_direction
		move.b #$00,p2_direction
		move.w #$0178,p2_xpos
		move.w #$0130,p2_ypos
		move.b #$00,p1_score
		move.b #$00,p2_score
        bsr setup_vdp
		bsr region_check
		bsr clear_vram
		bsr set_vsram
	;	bsr ld_splash		
		bsr load_font
		bsr load_palette
		bsr load_title_gfx	
		bsr title_loop
		bsr setup_sprites			
        move.w #$2300, sr       ;enable ints		
loop:
		cmpi.b #$00,vgmflag ;this shouldn't work, since the VGM flag gets overwritten by
		 beq loop			;the sprite buffer. It works though, so don't try to fix it.
		bsr music_driver
		bra loop
		
start_game_high_contrast:
		move.w #$8c89,(a3)
		bra start_game
		
start_game:
		move.b #$01,levelnumber
		bsr newstage
		bsr generate_map
		bsr load_hud
		bsr pal_boarder		
		rts
advance_stage:
		add.b #$01,levelnumber
		bsr newstage
		move.b #$03,p1_shooting	;Fixes no shooting bug.
		move.b #$03,p2_shooting	;Fixes no shooting bug.
		move.w #$0000,p1_shot_xpos
		move.w #$0000,p1_shot_ypos
		move.w #$0000,p2_shot_xpos ;offscreen the shot
		move.w #$0000,p2_shot_ypos		
		move.b #$00,vgmflag	
        move.w #$0000, p1_upvel
        move.w #$0000, p1_dnvel ;reset acceleration
        move.w #$0000, p1_ltvel 
        move.w #$0000, p1_rtvel	
	    move.w #$0000, p2_upvel
        move.w #$0000, p2_dnvel
        move.w #$0000, p2_ltvel 
        move.w #$0000, p2_rtvel
		move.w #$857c,(a3)		
		rts
kill_psg:
 		move.b #$9f,$c00011
        move.b #$df,$c00011
		move.b #$EF,$c00011	;kill psg	
        move.b #$FF,$c00011		
        move.b #$BF,$c00011	
 		rts
kill_2612: ;key off everything
        move.b #$28,$A04000
        move.b #$00,$A04001
        move.b #$01,$A04001
        move.b #$02,$A04001
        move.b #$03,$A04001
        move.b #$04,$A04001
        move.b #$05,$A04001
        move.b #$06,$A04001

        move.b #$08,$A04001
        move.b #$09,$A04001
        move.b #$0a,$A04001
        move.b #$0b,$A04001
        move.b #$0c,$A04001
        move.b #$0d,$A04001
        move.b #$0e,$A04001
		rts		
load_hud:
		move.l #$0000e082,d0
		bsr calc_vram
		move.l d0,(a3)	
		move.w #$e054,(a4) ;T
		move.w #$e049,(a4) ;I
		move.w #$e04d,(a4) ;M
		move.w #$e045,(a4) ;E
		move.w #$e02d,(a4) ;-		
		move.l #$0000e098,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e050,(a4) ;P		
		move.w #$e031,(a4) ;1				
		move.w #$e04b,(a4) ;K		
		move.w #$e049,(a4) ;I		
		move.w #$e04c,(a4) ;L		
		move.w #$e04c,(a4) ;L		
		move.w #$e053,(a4) ;S		
		move.w #$e03a,(a4) ;:
		move.w #$e07f,(a4) ;?
		move.w #$e07f,(a4) ;?	
		move.l #$0000e0ae,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e050,(a4) ;P		
		move.w #$e032,(a4) ;2			
		move.w #$e04b,(a4) ;K		
		move.w #$e049,(a4) ;I		
		move.w #$e04c,(a4) ;L		
		move.w #$e04c,(a4) ;L		
		move.w #$e053,(a4) ;S		
		move.w #$e03a,(a4) ;:	
		move.w #$e07f,(a4) ;?
		move.w #$e07f,(a4) ;?
		move.l #$0000e0c4,d0
		bsr calc_vram
		move.l d0,(a3)
  		move.w #$e04c,(a4) ;L	 		
  		move.w #$e056,(a4) ;V	 		
  		move.w #$e04c,(a4) ;L	 			 		
  		move.w #$e03a,(a4) ;:	 				
		rts

pal_boarder:
        move.l #$0000ee00,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0080,d4
		lea (pal_message), a5
		bsr block_txt_loop	
		rts		
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$a000,d4
        move.w d4,(a4)		
		bra termtextloop	

ld_splash:	
		bsr generate_map
 		lea (splashscreen), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
		lea (splash_palette), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop	
		move.w #$ffff,d4 ;splash screen timer		
		move.w #$0020,d5 ;ditto
splashwait:		
		dbf d4, splashwait
		dbf d5, splashwait	
		bsr clear_vram
		rts
title_loop:
		bsr read_controller1
		move.b d3,d7
	    or.b #$bf,d7
		cmpi.b #$bF,d7
		 beq load_info	;a
		move.b d3,d7		 
	    or.b #$ef,d7
		cmpi.b #$eF,d7
		 beq load_info	;b
		move.b d3,d7
	    or.b #$5f,d7
		cmpi.b #$5f,d7
		 beq start_game_high_contrast	 
		move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7f,d7
		 beq start_game	;start		 
		bra title_loop
		
load_info:
		lea (about_screen),a5
		move.w #$0700,d4
		move.l #$0000e000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr block_txt_loop	
		bsr unhold1
title_loop_info:
		bsr read_controller1
		move.b d3,d7
	    or.b #$bf,d7
		cmpi.b #$bF,d7
		 beq load_title_gfx2	;a
		move.b d3,d7		 
	    or.b #$ef,d7
		cmpi.b #$eF,d7
		 beq load_title_gfx2	;b
		move.b d3,d7		 
	    or.b #$df,d7
		cmpi.b #$dF,d7	
		 beq load_title_gfx2    ;c
		bra title_loop_info
		
load_title_gfx2:
		lea (title_art),a5
		move.w #$0700,d4
		move.l #$0000e000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr block_txt_loop
		bsr unhold1		
		bra title_loop	
		
			
 include "setup.asm"       ;run once as forget type stuff mostly 	
 include "decompress.asm"  ;kosinski decompression

read_controller1:               ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3,p1_input
		rts	
read_controller2:               ;d3 will have final controller reading!
		bra AI
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A1000b) ;Set direction
	    move.b  #$40, ($A10005) ;TH = 1
    	nop
	    nop
	    move.b  ($A10005), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10005)  ;TH = 0
	    nop
	    nop
	    move.b	($A10005), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3,p2_input
		rts			
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts			
block_txt_loop:		
		move.b (a5)+,d5
		andi.w #$00ff,d5
		eor.w #$e000,d5
        move.w d5,(a4)
		dbf d4, block_txt_loop
        rts	
unhold1:
		bsr read_controller1
		cmpi.b #$ff,d3
		 beq return
		bra unhold1	
unhold2:
		bsr read_controller2
		cmpi.b #$ff,d3
		 beq return
		bra unhold2			
		
newstage:		
		lea (stages),a0
		clr d0
		move.b levelnumber,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)
		
stages:
	dc.w ld_stage1
	dc.w ld_stage2
	dc.w ld_stage3
	dc.w ld_stage4
	dc.w ld_stage5
	dc.w ld_stage6
	dc.w ld_stage7
	dc.w ld_stage8
	dc.w ld_stage9
	dc.w ld_ending
	
ld_stage1:
        bsr kill_psg
		bsr kill_2612		
		lea (columnsVGM)+40,a2
		move.l a2, vgm_start		
		move.b #$ff,vgmflag	
 		lea (bg_windmill), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
		lea (palette_windmill), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage2:
        bsr kill_psg
		bsr kill_2612		
		lea (surferVGM)+40,a2
		move.l a2, vgm_start		
		move.b #$ff,vgmflag
 		lea (bg_desk), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_desk), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage3:
        bsr kill_psg
		bsr kill_2612		
		lea (castlevaniaVGM)+40,a2
		move.l a2, vgm_start		
		move.b #$ff,vgmflag	
 		lea (bg_rocks), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_rocks), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage4:	
        bsr kill_psg
		bsr kill_2612		
		lea (decapVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag			
 		lea (bg_logs), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_logs), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage5:
        bsr kill_psg
		bsr kill_2612		
		lea (clotho)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag				
 		lea (bg_house), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_house), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage6:
        bsr kill_psg
		bsr kill_2612
		lea (sttVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag				
 		lea (bg_cat), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_cat), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts
ld_stage7:
        bsr kill_psg
		bsr kill_2612		
		lea (rocketknightVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag				
 		lea (bg_headphone), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_phones), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts	
ld_stage8:
        bsr kill_psg
		bsr kill_2612		
		lea (vampire7VGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag				
 		lea (bg_cactus), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_cactus), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts		
ld_stage9:
        bsr kill_psg
		bsr kill_2612		
		lea (comixVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag				
 		lea (bg_sun), a5
		lea (ram_start), a1
        bsr decompress		
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9600,(a3)      ;00
		move.w #$977f,(a3)      ;ff
		move.l #$60000084,(a3)  ;DMA VRAM 2000
 		lea (palette_sun), a5
		move.w #$000f,d4
		move.l #$C0000000, (a3)
		bsr vram_loop		
        rts			
ld_ending:
        move.w #$8500,(a3) ;shut off sprites
		move.w #$2700,sr
		move.l d7, -(sp)
		move.l d5, -(sp)
		move.b p1_score, d7
		move.b p2_score, d5
		cmp.b d7,d5
		bgt ld_bad_ending
		bra ld_good_ending
ld_good_ending:		
		move.l (sp)+, d7	
		move.l (sp)+, d5
		lea (good_ending),a5
		move.w #$0700,d4
		move.l #$0000e000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr block_txt_loop
        bsr kill_psg
		lea (endVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag		
		bra good_end_loop
		
good_end_loop:
        bsr read_controller1
	    move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7f,d7
		 beq restart_game	
        bsr read_controller2
	    move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7f,d7
		 beq restart_game		
		bsr music_driver
		bra good_end_loop
		
ld_bad_ending:
		move.l (sp)+, d7	
		move.l (sp)+, d5
		lea (bad_ending),a5
		move.w #$0700,d4
		move.l #$0000e000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr block_txt_loop	
        bsr kill_psg
		lea (oozeVGM)+40,a2
		move.l a2, vgm_start
		move.b #$ff,vgmflag			
		bra bad_end_loop		
bad_end_loop:
        bsr read_controller1
	    move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7f,d7
		 beq restart_game	
        bsr read_controller2
	    move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7f,d7
		 beq restart_game
		bsr music_driver
		bra bad_end_loop
restart_game:
		move.w #$0000, p1_upvel
		move.w #$0000, p1_dnvel
		move.w #$0000, p1_ltvel
		move.w #$0000, p1_rtvel
		move.w #$0000, p2_upvel ;reset acceleration
		move.w #$0000, p2_dnvel
		move.w #$0000, p2_ltvel
		move.w #$0000, p2_rtvel	
set_stack:
		move.l #$FFFE00,a0 ;stage beginning
		move.w #$0050,d4	 ;clear 50 words
stack_loop:		
		move.w #$0000,-(a0)  ;clear stack
		dbf d4, stack_loop
		move.l #$FFFE00,a7 ;set stack pointer
		bra start
		
update_sprites:
        move.l #$0000f800,d0
		bsr calc_vram
		move.l d0,(a3)
		lea (spritebuffer),a0
		move.w #$003F,d4
update_loop:
		move.w (a0)+,(a4)
		dbf d4, update_loop	
		rts
		
animate_sprites_p1:
		cmpi.b #$00,p1_direction
		 beq p1faceleft
		cmpi.b #$01,p1_direction
		 beq p1faceright		 
		cmpi.b #$02,p1_direction
		 beq p1faceup
		cmpi.b #$03,p1_direction
		 beq p1facedown	
		rts 
p1faceleft:
		clr d7
		lea (spritebuffer),a5			
		move.w p1_ypos,(a5)+ ;ypos		
		move.w #$0f01,(a5)+
		move.w #$a580,(a5)+
		move.w p1_xpos,d7 ;xpos
		sub.w #$000f,d7   ;flip flop both sprites without changing overall position (makes collision detect easier)
		move.w d7,(a5)+ ;xpos
		
		move.w p1_ypos,(a5)+
		move.w #$0f02,(a5)+
		move.w #$a590,(a5)+
		move.w p1_xpos,d7
		add.w #$000F,d7
		move.w d7,(a5)+		
		rts
p1faceright:
		clr d7
		lea (spritebuffer),a5			
		move.w p1_ypos,(a5)+ ;ypos		
		move.w #$0f01,(a5)+
		move.w #$aD80,(a5)+
		move.w p1_xpos,d7 ;xpos
		add.w #$000f,d7
		move.w d7,(a5)+ ;xpos
		move.w p1_ypos,(a5)+
		move.w #$0f02,(a5)+
		move.w #$aD90,(a5)+
		move.w p1_xpos,d7
		sub.w #$000f,d7
		move.w d7,(a5)+		
		rts			
p1faceup:		
		clr d7
		lea (spritebuffer),a5			
		move.w p1_ypos,d7 ;ypos
        sub.w #$000f,d7		
		move.w d7,(a5)+ ;ypos		
		move.w #$0f01,(a5)+
		move.w #$a5a0,(a5)+
		move.w p1_xpos,(a5)+	
		move.w p1_ypos,d7
		add.w #$000f,d7
		move.w d7,(a5)+	
		move.w #$0f02,(a5)+
		move.w #$a5b0,(a5)+
		move.w p1_xpos,(a5)+	
		rts		
p1facedown:		
		clr d7
		lea (spritebuffer),a5			
		move.w p1_ypos,d7 ;ypos
        add.w #$000f,d7		
		move.w d7,(a5)+ ;ypos		
		move.w #$0f01,(a5)+
		move.w #$b5a0,(a5)+
		move.w p1_xpos,(a5)+	
		move.w p1_ypos,d7
		sub.w #$000f,d7
		move.w d7,(a5)+	
		move.w #$0f02,(a5)+
		move.w #$b5b0,(a5)+
		move.w p1_xpos,(a5)+	
		rts	

animate_sprites_p2:
		cmpi.b #$00,p2_direction
		 beq p2faceleft
		cmpi.b #$01,p2_direction
		 beq p2faceright		 
		cmpi.b #$02,p2_direction
		 beq p2faceup
		cmpi.b #$03,p2_direction
		 beq p2facedown	
		rts 
p2faceleft:
		clr d7
		lea (spritebuffer)+24,a5			
		move.w p2_ypos,(a5)+ ;ypos		
		move.w #$0f04,(a5)+
		move.w #$a5c0,(a5)+
		move.w p2_xpos,d7 ;xpos
		sub.w #$000f,d7
		move.w d7,(a5)+ ;xpos
		move.w p2_ypos,(a5)+
		move.w #$0f05,(a5)+
		move.w #$a5d0,(a5)+
		move.w p2_xpos,d7
		add.w #$000f,d7
		move.w d7,(a5)+		
		rts
p2faceright:
		clr d7
		lea (spritebuffer)+24,a5			
		move.w p2_ypos,(a5)+ ;ypos		
		move.w #$0f04,(a5)+
		move.w #$aDc0,(a5)+
		move.w p2_xpos,d7 ;xpos
		add.w #$000f,d7
		move.w d7,(a5)+ ;xpos
		move.w p2_ypos,(a5)+
		move.w #$0f05,(a5)+
		move.w #$aDd0,(a5)+
		move.w p2_xpos,d7
		sub.w #$000f,d7
		move.w d7,(a5)+		
		rts			
p2faceup:		
		clr d7
		lea (spritebuffer)+24,a5			
		move.w p2_ypos,d7 ;ypos	
		sub.w #$000f,d7
		move.w d7,(a5)+ ;ypos		
		move.w #$0f04,(a5)+
		move.w #$a5e0,(a5)+
		move.w p2_xpos,(a5)+	
		move.w p2_ypos,d7
		add.w #$000f,d7
		move.w d7,(a5)+	
		move.w #$0f05,(a5)+
		move.w #$a5f0,(a5)+
		move.w p2_xpos,(a5)+	
		rts		
p2facedown:		
		clr d7
		lea (spritebuffer)+24,a5			
		move.w p2_ypos,d7 ;ypos	
		add.w #$000f,d7
		move.w d7,(a5)+ ;ypos		
		move.w #$0f04,(a5)+
		move.w #$b5e0,(a5)+
		move.w p2_xpos,(a5)+	
		move.w p2_ypos,d7
		sub.w #$000f,d7
		move.w d7,(a5)+	
		move.w #$0f05,(a5)+
		move.w #$b5f0,(a5)+
		move.w p2_xpos,(a5)+	
		rts	

		
check_input_p2:	
		move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7F,d7
		 beq pause2
		 
		move.b d3,d7
	    or.b #$bf,d7
		cmpi.b #$bF,d7
		 beq p2_shoot	;a
p2_retshoot:		
	   move.b d3,d7
	   or.b #$df,d7 ;c
	   cmpi.b #$dF,d7	
		beq p2_lock_vert
	   move.b #$00,p2_vertlock
p2_ret:		   
		move.b d3,d7		 
	    or.b #$ef,d7 ;b
		cmpi.b #$eF,d7		
 		beq p2_lock_horiz    
	   move.b #$00,p2_horizlock		
p2_ret2:		
	   move.b d3,d7
	   or.b #$fb,d7
	   cmpi.b #$fb,d7
	    beq p2_left
ret00:		
	   move.b d3,d7
	   or.b #$f7,d7
	   cmpi.b #$f7,d7
	    beq p2_right
ret11:		
	   move.b d3,d7
	   or.b #$fe,d7
	   cmpi.b #$fe,d7
	    beq p2_up
ret22:		
	   move.b d3,d7
	   or.b #$fd,d7
	   cmpi.b #$fd,d7
        beq p2_down
	   rts
p2_lock_vert:
	   move.w #$0000,p2_upvel
	   move.w #$0000,p2_dnvel
	   move.b #$ff,p2_vertlock
	   bra p2_ret
p2_lock_horiz:
	   move.w #$0000,p2_ltvel
	   move.w #$0000,p2_rtvel
	   move.b #$ff,p2_horizlock
	   bra p2_ret2	   
	   
p2_left:
	   move.b #$00, p2_direction
       add.w #$01,p2_ltvel
	   bra ret00
p2_right: 
	   move.b #$01, p2_direction
       add.w #$01,p2_rtvel
 	   bra ret11
p2_up:
       add.w #$01,p2_upvel
	   move.b #$02, p2_direction 
	   bra ret22
p2_down:
	   move.b #$03, p2_direction 
       add.w #$01,p2_dnvel  
	   rts		
check_input_p1:
		move.b d3,d7
	    or.b #$7f,d7
		cmpi.b #$7F,d7
		 beq pause1
		 
		move.b d3,d7
	    or.b #$bf,d7
		cmpi.b #$bF,d7
		 beq p1_shoot	;a
p1_retshoot:		
	   move.b d3,d7
	   or.b #$df,d7 ;c
	   cmpi.b #$dF,d7	
		beq p1_lock_vert
	   move.b #$00,p1_vertlock
p1_ret:		   
		move.b d3,d7		 
	    or.b #$ef,d7 ;b
		cmpi.b #$eF,d7		
 		beq p1_lock_horiz    
	   move.b #$00,p1_horizlock		
p1_ret2:		
	   move.b d3,d7
	   or.b #$fb,d7
	   cmpi.b #$fb,d7
	    beq p1_left
ret0:		
	   move.b d3,d7
	   or.b #$f7,d7
	   cmpi.b #$f7,d7
	    beq p1_right
ret1:		
	   move.b d3,d7
	   or.b #$fe,d7
	   cmpi.b #$fe,d7
	    beq p1_up
ret2:		
	   move.b d3,d7
	   or.b #$fd,d7
	   cmpi.b #$fd,d7
        beq p1_down
	   rts
p1_lock_vert:
	   move.w #$0000,p1_upvel
	   move.w #$0000,p1_dnvel
	   move.b #$ff,p1_vertlock
	   bra p1_ret
p1_lock_horiz:
	   move.w #$0000,p1_ltvel
	   move.w #$0000,p1_rtvel
	   move.b #$ff,p1_horizlock
	   bra p1_ret2	   
	   
p1_left:
	   move.b #$00, p1_direction
       add.w #$01,p1_ltvel
	   bra ret0
p1_right: 
	   move.b #$01, p1_direction
       add.w #$01,p1_rtvel
 	   bra ret1
p1_up:
       add.w #$01,p1_upvel
	   move.b #$02, p1_direction 
	   bra ret2
p1_down:
	   move.b #$03, p1_direction 
       add.w #$01,p1_dnvel  
	   rts
	 
pause1:
		bsr kill_2612
		bsr kill_psg
		move.w sr, -(sp)
		move.w #$2700,sr
		move.l #$0000e7a0,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$6050,(a4) ;P1 Paused
		move.w #$6031,(a4)
		move.w #$6020,(a4)
		move.w #$6050,(a4)
		move.w #$6061,(a4)
		move.w #$6075,(a4)
		move.w #$6073,(a4)
		move.w #$6065,(a4)
		move.w #$6064,(a4)	
		bsr unhold1		
pauseloop1:
        bsr read_controller1
		cmpi.b #$7F,d3
		 bne pauseloop1	
unpause1:		 
		bsr unhold1	
		move.l #$0000e7a0,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0368,(a4)
		move.w #$0369,(a4)
		move.w #$036a,(a4)
		move.w #$036b,(a4)
		move.w #$036c,(a4)
		move.w #$036d,(a4)
		move.w #$036e,(a4)
		move.w #$036f,(a4)
		move.w #$0370,(a4)	
		move.w (sp)+,sr			
		rts
pause2:
		bsr kill_2612
		bsr kill_psg
		move.w sr, -(sp)
		move.w #$2700,sr
		move.l #$0000e7a0,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$6050,(a4) ;P2 Paused
		move.w #$6032,(a4)
		move.w #$6020,(a4)
		move.w #$6050,(a4)
		move.w #$6061,(a4)
		move.w #$6075,(a4)
		move.w #$6073,(a4)
		move.w #$6065,(a4)
		move.w #$6064,(a4)			
		bsr unhold2		
pauseloop2:
        bsr read_controller2
		cmpi.b #$7F,d3
		 bne pauseloop2	
unpause2:		 
		bsr unhold2	
		move.l #$0000e7a0,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0368,(a4)
		move.w #$0369,(a4)
		move.w #$036a,(a4)
		move.w #$036b,(a4)
		move.w #$036c,(a4)
		move.w #$036d,(a4)
		move.w #$036e,(a4)
		move.w #$036f,(a4)
		move.w #$0370,(a4)		
		move.w (sp)+,sr			
		rts		
	   
p1_shoot:
		cmpi.b #$04,p1_shooting ;are we already shooting?
		 bne p1_retshoot
        move.b p1_direction,p1_shooting
		move.w p1_ypos,p1_shot_ypos
		move.w p1_xpos,p1_shot_xpos				
		add.w #$000b,p1_shot_ypos ;align shot to shoot out middle of ship
		add.w #$000b,p1_shot_xpos ;ditto
		lea (good_text),a1
		bra p1_retshoot		
p2_shoot:
		cmpi.b #$04,p2_shooting ;are we already shooting?
		 bne p2_retshoot
        move.b p2_direction,p2_shooting
		move.w p2_ypos,p2_shot_ypos
		move.w p2_xpos,p2_shot_xpos				
		add.w #$000b,p2_shot_ypos ;align shot to shoot out middle of ship
		add.w #$000b,p2_shot_xpos ;ditto
		lea (evil_text),a6
		bra p2_retshoot	
 include "p2_shot.asm"		
 include "p1_shot.asm"	

 include "deceleration.asm" 
		
p1_acceleration:
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p1_rtvel,d0
		move.w p1_ltvel,d1
		lsr #$0002,d0       ;adjust to a slower speed
		lsr #$0002,d1		
		add.w d0,p1_xpos
		sub.w d1,p1_xpos	
        move.w p1_dnvel,d0
		move.w p1_upvel,d1
		lsr #$0002,d0
		lsr #$0002,d1				
		add.w d0,p1_ypos
		sub.w d1,p1_ypos	
		cmpi.w #$0045,p1_ypos
		ble p1_toohigh	
		cmpi.w #$0180,p1_ypos
		bge p1_toolow
		cmpi.w #$0030,p1_xpos
		ble p1_tooleft
		cmpi.w #$01e0,p1_xpos
		bge p1_tooright			
		movem.l (sp)+,d0/d1		
        rts  	   
p1_toohigh:
		move.w #$0160,p1_ypos
		movem.l (sp)+,d0/d1		
        rts	   
p1_toolow:
		move.w #$0060,p1_ypos
		movem.l (sp)+,d0/d1		
        rts	   
p1_tooleft:
		move.w #$01D0,p1_xpos
		movem.l (sp)+,d0/d1		
        rts	
p1_tooright:
		move.w #$0050,p1_xpos
		movem.l (sp)+,d0/d1		
        rts	
p2_acceleration:
		movem.l d0/d1, -(sp)
		clr d0
		clr d1
        move.w p2_rtvel,d0
		move.w p2_ltvel,d1
		lsr #$0002,d0       ;adjust to a slower speed
		lsr #$0002,d1		
		add.w d0,p2_xpos
		sub.w d1,p2_xpos	
        move.w p2_dnvel,d0
		move.w p2_upvel,d1
		lsr #$0002,d0
		lsr #$0002,d1				
		add.w d0,p2_ypos
		sub.w d1,p2_ypos	
		cmpi.w #$0045,p2_ypos
		ble p2_toohigh	
		cmpi.w #$0180,p2_ypos
		bge p2_toolow
		cmpi.w #$0030,p2_xpos
		ble p2_tooleft
		cmpi.w #$01e0,p2_xpos
		bge p2_tooright			
		movem.l (sp)+,d0/d1		
        rts	
p2_toohigh:
		move.w #$0160,p2_ypos
		movem.l (sp)+,d0/d1		
        rts	   
p2_toolow:
		move.w #$0060,p2_ypos
		movem.l (sp)+,d0/d1		
        rts	   
p2_tooleft:
		move.w #$01D0,p2_xpos
		movem.l (sp)+,d0/d1		
        rts	
p2_tooright:
		move.w #$0050,p2_xpos

		movem.l (sp)+,d0/d1		
        rts			
return:
		rts
returnint:
		rte
HBlank:
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000
	    move.b (a5)+,$A04001
        add.w #$01,hblanks
		cmpi.b #$01, killed
		beq skew_p1
		cmpi.b #$02, killed
		beq skew_p2		
        rte
skew_p1:
		movem.l d0/d1, -(sp) ;player 1 kill
	    move.w hblanks,d0      ;increase skew the farther down
	    muls.w skew_counter,d0   ;might be able to use MULU also
	    move.b #$04,d1
	    lsr d1,d0              ;speed adjustment
	    ;move.l #$78000003,(a3) ;vram F800
		move.w d0,(a4)		   ;sprite1		
	    move.l #$78060003,(a3) ;vram F806
		move.w d0,(a4)		   ;sprite1
	    move.l #$780e0003,(a3) ;vram F80e
		move.w d0,(a4)		   ;sprite2		
		movem.l (sp)+,d0/d1			
	    rte	
skew_p2:
		movem.l d0/d1, -(sp) ;player 2 kill
	    move.w hblanks,d0
	    muls.w skew_counter,d0
	    move.b #$04,d1
	    lsr d1,d0 
	    ;move.l #$78180003,(a3)
		move.w d0,(a4)
	    move.l #$781e0003,(a3)
		move.w d0,(a4)
	    move.l #$78260003,(a3)
		move.w d0,(a4)
		movem.l (sp)+,d0/d1			
	    rte	
		
register_hit:
		move.b #$00,vgmflag
		bsr kill_psg
		bsr kill_2612
		move.w #$00a8,p1_xpos
		move.w #$0098,p1_ypos
		move.b #$01,p1_direction
		move.b #$00,p2_direction
		move.w #$0178,p2_xpos
		move.w #$0130,p2_ypos
		lea (boom),a5
		move.w #$8a00,(a3)			  ;enable horiz ints
        move.w #$0000, skew_counter ;controls skew starting point
		move.b #$ff, scale_done
		move.w #$0000,hblanks
		move.b #$ff,raster_flag
		rts	
noscale:
		move.w #$857e,(a3)
		move.b #$00,killed
		move.w #$8aff, (a3)    ;no more h ints			
		move.b #$00,raster_flag
		bsr advance_stage
		rte
calc_script:
		move.w #$0000,hblanks
	    add.w #$0001,skew_counter
	    cmpi.w #$00A0,skew_counter		
	    beq noscale
	    rte 		
VBlank:
		cmpi.b #$ff,raster_flag
		beq calc_script
forward:		
		bsr region_check       ;fuck your region switch
		eor.b #$ff,inverter
        bsr read_controller1
		bsr check_input_p1
		bsr p1_acceleration			
		bsr p1_deceleration			
        bsr read_controller2
		
		bra AI_skip
		
		bsr check_input_p2		
		bsr p2_acceleration	
		bsr p2_deceleration	
AI_skip:		
		bsr increment_clock
		bsr draw_hud
		bsr animate_sprites_p1
		bsr animate_sprites_p2
		bsr animatep1_shot
		clr d6                 ;must be between shot animation routine calls
	    bsr animatep2_shot	
		bsr update_sprites
		bsr check_p1_score     ;collision detection p1
		bsr check_p2_score
        rte	
	
	
    include "collision_detect.asm"
    include "AI.asm"
	
increment_clock:
	include "clock.asm"
draw_hud:
	include "hud_draw.asm"
	include "crashscreen.asm"
	include "music_driver.asm"
    include "data.asm"
	
	dc.b "Lithium to bite the burger - Pass it on!"

ROM_End:
              
              
