animatep2_shot:	
		cmpi.b #$00, p2_shooting
		 beq p2_shoot_anim_left		 
		cmpi.b #$01, p2_shooting
		 beq p2_shoot_anim_right
		cmpi.b #$02, p2_shooting
		 beq p2_shoot_anim_up
		cmpi.b #$03, p2_shooting
		 beq p2_shoot_anim_down			 
		bsr p2_noshoot          ;fixed shooting bug
		rts 
p2_shoot_anim_left:
		sub.w #$0006,p2_shot_xpos	
		lea (spritebuffer)+40,a5		
		move.w p2_shot_ypos,(a5)+
		move.w #$0000,(a5)+ ;link
		
		move.b (a6)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+	
		
		move.w p2_shot_xpos,(a5)+
		andi.w #$0fff,p2_shot_xpos			
		cmpi.w #$0070, p2_shot_xpos
		 ble p2_noshoot
		rts
p2_shoot_anim_right:
		add.w #$0006,p2_shot_xpos	
		lea (spritebuffer)+40,a5		
		move.w p2_shot_ypos,(a5)+
		move.w #$0000,(a5)+
		
		move.b (a6)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+		
	
		move.w p2_shot_xpos,(a5)+
		andi.w #$0fff,p2_shot_xpos			
		cmpi.w #$01c0, p2_shot_xpos		
		 bge p2_noshoot
		rts	
p2_shoot_anim_up:
		sub.w #$0006,p2_shot_ypos	
		lea (spritebuffer)+40,a5		
		move.w p2_shot_ypos,(a5)+
		move.w #$0000,(a5)+
		
		move.b (a6)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+		

		move.w p2_shot_xpos,(a5)+
		andi.w #$0fff,p2_shot_ypos			
		cmpi.w #$0070, p2_shot_ypos		
		 ble p2_noshoot
		rts	
p2_shoot_anim_down:
		add.w #$0006,p2_shot_ypos	
		lea (spritebuffer)+40,a5		
		move.w p2_shot_ypos,(a5)+
		move.w #$0000,(a5)+
		
		move.b (a6)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+		
			
		move.w p2_shot_xpos,(a5)+
		andi.w #$0fff,p2_shot_ypos			
		cmpi.w #$0160, p2_shot_ypos		
		 bge p2_noshoot
		rts			
		
p2_noshoot:
		move.w #$0000, p2_shot_xpos
		move.w #$0000, p2_shot_ypos
        move.b #$04,p2_shooting		
		rts
		