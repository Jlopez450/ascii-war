ram_start = $FF0000
p1_xpos = $FFFF00
p1_ypos = $FFFF02 
p2_xpos = $FFFF04
p2_ypos = $FFFF06
levelnumber = $FFFF08
vblanks = $FFFF0A
seconds = $FFFF0C
minutes = $FFFF0E
region = $FFFF10
p1_score = $FFFF12
p2_score = $FFFF14
p1_input = $FFFF16
p2_input = $FFFF18
p1_direction = $FFFF1a ;0=left 1=right 2=up 3=down
p2_direction = $FFFF1c
p1_upvel = $FFFF1e
p1_dnvel = $FFFF20
p1_ltvel = $FFFF22
p1_rtvel = $FFFF24
p2_upvel = $FFFF26
p2_dnvel = $FFFF28
p2_ltvel = $FFFF2a
p2_rtvel = $FFFF2c
hblanks = $FFFF2e
skewamount = $FFFF30
skew_counter = $FFFF32
raster_flag = $FFFF34
scale_done = $FFFF36
hitflag = $FFFF38 ;0=no 1=p1 hit 2=p2 hit 3=double hit
p1_vertlock = $FFFF3c
p1_horizlock = $FFFF3e
p1_shooting = $FFFb48 ;0=left 1=right 2=up 3=down 4=no
p2_shooting = $FFFbD2 ;0=left 1=right 2=up 3=down 4=no
p1_shot_xpos = $FFFB42
p1_shot_ypos = $FFFB46
p2_shot_xpos = $FFFBae
p2_shot_ypos = $FFFBb2
killed = $FFFD00 ;0=no kill 1=p1 dead 2=p2 dead
spritebuffer = $FFFFA0
vgmflag = $FFFFA2   ;broken because of sprite buffer
vgm_start = $FFFDa4 ;long word
p2_vertlock = $FFFFaa
p2_horizlock = $FFFFac
inverter = $FFFEac

D0_temp = $FFAF00
D1_temp = $FFAF04
D2_temp = $FFAF08
D3_temp = $FFAF0c
D4_temp = $FFAF10
D5_temp = $FFAF14
D6_temp = $FFAF18
D7_temp = $FFAF1c
A0_temp = $FFAF20
A1_temp = $FFAF24
A2_temp = $FFAF28
A3_temp = $FFAF2c
A4_temp = $FFAF30
A5_temp = $FFAF34
A6_temp = $FFAF38
A7_temp = $FFAF3c
sr_temp = $FFAF40
pc_temp = $FFAF44
errorID = $FFAF50
