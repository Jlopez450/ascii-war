AI:
		move.b #$FF,d3
		move.w p1_xpos,d0
		move.w p2_xpos,d1		
		cmp.w d0,d1
		bge ai_left	
		bra ai_right
ai_ret:	
		move.w p1_ypos,d0
		move.w p2_ypos,d1	
		cmp.w d0,d1
		bge ai_up
		bra ai_down
ai_left:
		move.b #$00,p2_direction
		sub.w #$3,p2_xpos
		bra ai_ret
ai_right:
		move.b #$01,p2_direction
		add.w #$3,p2_xpos
		bra ai_ret	
ai_up:
		move.b #$02,p2_direction
		sub.w #$3,p2_ypos
		rts
ai_down:
		move.b #$03,p2_direction
		add.w #$3,p2_ypos
		rts
ai_fire:
		eor.b #$40,d3
		rts