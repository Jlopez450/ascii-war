animatep1_shot:	
		cmpi.b #$00, p1_shooting
		 beq p1_shoot_anim_left		 
		cmpi.b #$01, p1_shooting
		 beq p1_shoot_anim_right
		cmpi.b #$02, p1_shooting
		 beq p1_shoot_anim_up
		cmpi.b #$03, p1_shooting
		 beq p1_shoot_anim_down			 
		bsr p1_noshoot          ;fixed shooting bug
		rts 
p1_shoot_anim_left:
		sub.w #$0006,p1_shot_xpos	
		lea (spritebuffer)+16,a5		
		move.w p1_shot_ypos,(a5)+
		move.w #$0003,(a5)+
		
		move.b (a1)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+	
		;move.w #$e02a,(a5)+	
		
		move.w p1_shot_xpos,(a5)+
		andi.w #$0fff,p1_shot_xpos			
		cmpi.w #$0070, p1_shot_xpos
		 ble p1_noshoot
		rts
p1_shoot_anim_right:
		add.w #$0006,p1_shot_xpos
		lea (spritebuffer)+16,a5		
		move.w p1_shot_ypos,(a5)+
		move.w #$0003,(a5)+
		
		move.b (a1)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+		
	
		;move.w #$e02a,(a5)+	
		move.w p1_shot_xpos,(a5)+
		andi.w #$0fff,p1_shot_xpos			
		cmpi.w #$01c0, p1_shot_xpos		
		 bge p1_noshoot
		rts	
p1_shoot_anim_up:
		sub.w #$0006,p1_shot_ypos	
		lea (spritebuffer)+16,a5		
		move.w p1_shot_ypos,(a5)+
		move.w #$0003,(a5)+
		
		move.b (a1)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+	
		
		;move.w #$e02a,(a5)+	
		move.w p1_shot_xpos,(a5)+
		andi.w #$0fff,p1_shot_ypos			
		cmpi.w #$0070, p1_shot_ypos		
		 ble p1_noshoot
		rts	
p1_shoot_anim_down:
		add.w #$0006,p1_shot_ypos	
		lea (spritebuffer)+16,a5		
		move.w p1_shot_ypos,(a5)+
		move.w #$0003,(a5)+
		
		move.b (a1)+,d6
		eor.w #$e000,d6
		move.w d6,(a5)+		
		
		;move.w #$e02a,(a5)+	
		move.w p1_shot_xpos,(a5)+
		andi.w #$0fff,p1_shot_ypos			
		cmpi.w #$0160, p1_shot_ypos		
		 bge p1_noshoot
		rts			
		
p1_noshoot:
		move.w #$0000, p1_shot_xpos
		move.w #$0000, p1_shot_ypos
        move.b #$04,p1_shooting		
		rts	