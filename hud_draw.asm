        clr d7
		move.b seconds, d7		;clock
		andi.b #$f0, d7
		lsr.b #$04, d7
		add.b #$30,d7           ;convert to ASCII
		move.l #$60920003, (a3) ;vram write $E092
		andi.w #$00ff,d7
		eor.w #$e000,d7         ;use last palette			
		move.w d7, (a4)		
		clr d7
		move.b seconds, d7
		andi.b #$0f, d7
		add.b #$30,d7
		move.l #$60940003, (a3) ;vram write $E094
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)	
        clr d7
		move.b minutes, d7
		andi.b #$f0, d7
		lsr.b #$04, d7
		add.b #$30,d7
		move.l #$608c0003, (a3) ;vram write $E08c	
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)		
		clr d7
		move.b minutes, d7
		andi.b #$0f, d7
		add.b #$30,d7
		move.l #$608e0003, (a3) ;vram write $E08e
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)	

		move.w #$e03a,(a4)   ;colon
		
        clr d7
		move.b p1_score, d7
		andi.b #$f0, d7
		lsr.b #$04, d7
		add.b #$30,d7
		move.l #$60a80003, (a3) ;vram write $E0a8
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)		
		clr d7
		move.b p1_score, d7
		andi.b #$0f, d7
		add.b #$30,d7
		move.l #$60aa0003, (a3) ;vram write $E0aa
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)
		
        clr d7
		move.b p2_score, d7
		andi.b #$f0, d7
		lsr.b #$04, d7
		add.b #$30,d7
		move.l #$60be0003, (a3) ;vram write $E0be
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)		
		clr d7
		move.b p2_score, d7
		andi.b #$0f, d7
		add.b #$30,d7
		move.l #$60c00003, (a3) ;vram write $E0c0
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)		

		move.b levelnumber, d7
		andi.b #$0f, d7
		add.b #$30,d7
		move.l #$60cc0003, (a3) ;vram write $E0cc
		andi.w #$00ff,d7
		eor.w #$e000,d7 		
		move.w d7, (a4)			
        rts	