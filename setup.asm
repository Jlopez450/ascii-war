load_palette:
		move.w #$003F,d4
		lea (palette),a5
        move.l #$C0000000,(a3)
		bsr vram_loop
        move.l #$C0200000,(a3)
		move.w #$0eee,d2		;palette for players
		move.w #$000f,d4
palette_loop:
		move.w d2,(a4)
		dbf d4, palette_loop
		rts
load_title_gfx:
		lea (title_art),a5
		move.w #$0700,d4
		move.l #$0000e000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr block_txt_loop
		rts		
load_font:
		lea (font),a5
		move.w #$0800,d4
        move.l  #$40000000,(a3) ;set VRAM write $0000		
		bsr vram_loop 
		rts
setup_vdp:
		move   #$2700, sr       ;disable ints
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts 
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
		move.w #$7FF0,d4
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts	
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts
generate_map:                  ;generate a map for 320x224 images
        move.l #$00000100,d0         ;first tile
        move.w #$001c,d5
        move.l #$60000003,(a3) ;vram write to $E000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$00,(a4)
        dbf d4,maploop2
        dbf d5,superloop
        rts
setup_sprites:
		move.l #$0000b000,d0
		bsr calc_vram
		move.l d0,(a3)
		lea (sprites),a5
		move.w #$0900,d4
		bsr vram_loop
		lea (spritebuffer),a5
		move.w #$0008,d4
		clr d0
sprite_loop:
	    move.w #$0000,(a5)+
		add.w #$0001,d0
		move.w d0,(a5)+
	    move.w #$0000,(a5)+		
	    move.w #$0000,(a5)+		
		dbf d4, sprite_loop	
		rts
clear_ram:
		move.l #$FFFF00,a0
		move.w #$0070,d4
ram_loop:
		move.w #$0000,(a0)+
		dbf d4, ram_loop
		rts		
		
region_check:
		clr d0
        move.b  $A10001, d0
		andi.b #$40, d0
		cmpi.b #$40, d0
		 beq pal 
		bra ntsc
pal:
		move.b #$50, region
		move.w #$817c,(a3)
		rts
ntsc:				
        move.b #$60, region
		rts	
set_vsram:
        move.l #$40000010,(a3)   ;write to vsram    
		move.w #$0000,(a4)
		nop
		move.w #$0000,(a4)
		rts
clear_regs:
		move.l #$00000000,d0
		move.l #$00000000,d1
		move.l #$00000000,d2
		move.l #$00000000,d3
		move.l #$00000000,d4
		move.l #$00000000,d5
		move.l #$00000000,d6
		move.l #$00000000,d7
		move.l #$00000000,a0
		move.l #$00000000,a1
		move.l #$00000000,a2
		move.l #$00000000,a3
		move.l #$00000000,a4
		move.l #$00000000,a5
		move.l #$00000000,a6
		rts

		
		
