check_p1_score:
		movem d3/d4/d5/d6, -(sp)		
		move.w p2_xpos, d3
		move.w p2_ypos, d4		
		move.w p1_shot_xpos, d5
		move.w p1_shot_ypos, d6
		
		cmpi.b #$00,p2_direction
		 beq p1_score_horiz
		cmpi.b #$01,p2_direction ;Enemy is facing horizontally, else is vertical.
		 beq p1_score_horiz
		 	 
        cmp.w d3,d5
		 ble nohit
		add.w #$20,d3  ;compensate for ship width
        cmp.w d3,d5
		 bge nohit
		 
		sub.w #$12,d4			 
        cmp.w d4,d6
		 ble nohit
		add.w #$40,d4  ;compensate for ship length
        cmp.w d4,d6
		 bge nohit			 
		 bsr p1_add_score
		 
p1_score_horiz:
		sub.w #$12,d3
        cmp.w d3,d5
		 ble nohit
		add.w #$40,d3
        cmp.w d3,d5
		 bge nohit	
		 
		sub.w #$0a,d4		 
        cmp.w d4,d6
		 ble nohit
		add.w #$26,d4
        cmp.w d4,d6
		 bge nohit			 
		 bsr p1_add_score	 	
nohit:
		movem (sp)+, d3/d4/d5/d6
		rts
check_p2_score:
		movem d3/d4/d5/d6, -(sp)		
		move.w p1_xpos, d3
		move.w p1_ypos, d4		
		move.w p2_shot_xpos, d5
		move.w p2_shot_ypos, d6
		
		cmpi.b #$00,p1_direction
		 beq p2_score_horiz
		cmpi.b #$01,p1_direction ;Enemy is facing horizontally, else is vertical.
		 beq p2_score_horiz
		 	 
        cmp.w d3,d5
		 ble nohit
		add.w #$20,d3  ;compensate for ship width
        cmp.w d3,d5
		 bge nohit
		 
		sub.w #$12,d4			 
        cmp.w d4,d6
		 ble nohit
		add.w #$40,d4  ;compensate for ship length
        cmp.w d4,d6
		 bge nohit			 
		 bsr p2_add_score
		 
p2_score_horiz:
		sub.w #$12,d3
        cmp.w d3,d5
		 ble nohit
		add.w #$40,d3
        cmp.w d3,d5
		 bge nohit	
		 
		sub.w #$0a,d4		 
        cmp.w d4,d6
		 ble nohit
		add.w #$26,d4
        cmp.w d4,d6
		 bge nohit		 
		 bsr p2_add_score	 	
		movem (sp)+, d3/d4/d5/d6
		rts		
		
p1_add_score:
		add.b #$01,p1_score
		move.b #$02, killed
		bsr register_hit
		rts
p2_add_score:
		add.b #$01,p2_score
		move.b #$01, killed
		bsr register_hit
		rts	