music_driver:
		;cmpi.b #$00,vgmflag
		;beq return
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   		
vgm_loop:
		bsr test2612
		clr d2
        move.b (a2)+,d2
		cmpi.b #$61,d2
		 beq wait
		 cmpi.b #$66,d2
		 beq loop_playback
		;cmpi.b #$4f,d2  ;game gear stereo, ignore on GEN/MD
		; beq stereo
		cmpi.b #$52,d2 
		 beq update2612_0
		cmpi.b #$53,d2 
		 beq update2612_1
		cmpi.b #$50,d2
		 beq update_psg
		rts     
update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra vgm_loop
		
update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra vgm_loop
		
loop_playback:
 		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011
		bsr kill_2612
        move.l vgm_start,a2
		rts
stop_loop:
        move.b d4, (a2)
		move.b #$00, (a3)
		move.b d4, (a4)
		move.b #$00, (a5)
		add.b #$01,d4
        dbf	d5,stop_loop
hang		
		bra hang
	
stereo:
        move.b (a2)+,d2
		bra vgm_loop
	
update_psg:
        move.b (a2)+,$C00011
		bra vgm_loop
	
wait:		
		clr d2
		clr d1
		move.b (a2)+,d1
		move.b (a2)+,d2			
		lsl.w #$08,d2    ;switch to big endian
		eor.w d2,d1      ;ditto
		;lsl #$01,d1     ;tempo adjust (removed in favor of more NOPs)
		bra waitloop
		
waitloop:
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		nop		
		nop		
		nop		
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		;nop
		;nop	
		dbf d1,waitloop
		cmpi.b #$00,vgmflag
		beq return		
		rts
test2612:
		clr d2
        move.b $A04001,d2
		andi.b #$80,d2
        cmpi.b #$80,d2
		beq test2612 
		rts				
		
      